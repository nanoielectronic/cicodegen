<?php
$controller_codeview = '<?php
defined(\'BASEPATH\') or exit(\'No direct script access allowed\');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->login_view();
    }

    public function login_view()
    {
        //@VIEW
        $this->load->view(\'login/login_view\');
    }

    public function login_fail_view()
    {
        //@VIEW
        $this->load->view(\'login/login_fail_view\');
    }

    public function check_login()
    {

        $post = $this->input->post(NULL, TRUE);

        if (isset($post[\'username\']) && isset($post[\'password\'])) {

            $user = $this->db->get_where(\'USER_TABLE\', array(\'username\' => $post[\'username\']))->row();
            if (count($user) > 0) {
                if ($user->password == $post[\'password\']) {
                    $sdata = array(
                        \'is_login\' => TRUE,
                        \'id\' => $user->id,
                        \'username\' => $post[\'username\'],
                        \'fullname\' => $user->fullname,
                        \'login_time\' => date(\'Y-m-d H:i:s\'),
                    );
                    $this->session->set_userdata($sdata);
                    redirect(\'YOUR HOME PAGE\');
                } else {
                    redirect(\'login/login_fail_view\');
                }
            } else {
                redirect(\'login/login_fail_view\');
            }
        } else {
            redirect(\'login/login_fail_view\');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(\'home\');
    }
}//End class
';

?>
<h5><i class="fa fa-dot-circle-o" aria-hidden="true"></i> application/controllers/Login.php</h5>
<pre class="line-numbers language-php" ><code><?php echo htmlspecialchars($controller_codeview); ?></code></pre>
