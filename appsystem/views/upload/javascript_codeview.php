<?php
$javascript_codeview = 'Dropzone.options.myAwesomeDropzone = false;
Dropzone.autoDiscover = false;
Dropzone.options.previewa = {
    init: function() {
        Dropzone.options.previewaDropzone = false;
    }
};
$(document).ready(function() {

    $("#div_upload").dropzone({
        url:site_url+\'upload/upload_process\',
        autoProcessQueue: false,
        maxFilesize:10,
        maxFiles: 5,
        uploadMultiple: true,
        paramName: "files",
        parallelUploads: 10,
        addRemoveLinks: true,
        acceptedFiles: ".jpg, .png, .gif",
        init: init_upload

    });

});//END READY

function init_upload() {
    var upload_file = this;
    //init event click upload
    $(\'#btn_upload\').on("click", function() {

        if(upload_file.files.length > 0){
            show_preload();
            upload_file.processQueue();
        }else {
            Swal.fire({
                type: \'warning\',
                title: \'Warning!\',
                html: \'Please input a file for upload!\'
            });
        }
    });
    //Success upload
    this.on("successmultiple", function(file, res, data) {
        var resp = JSON.parse(res);
        hide_preload();
        if(resp.is_success){
            Swal.fire({
                type: \'success\',
                title: \'Complete\',
                html: resp.msg
            }).then(function(){
                window.location.reload();
            });
        }else {
            Swal.fire({
                type: \'warning\',
                title: \'Warning!\',
                html: resp.msg
            });
        }
    });

}//END FUNCTION
'
?>
<h5>
    <i class="fa fa-dot-circle-o" aria-hidden="true"></i> appjs/upload/app.js
</h5>
<pre class="line-numbers language-javascript" ><code><?php echo htmlspecialchars($javascript_codeview); ?></code></pre>
