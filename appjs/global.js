var site_url = $('meta[name="site_url"]').attr('content');
var base_url = $('meta[name="base_url"]').attr('content');

$('[data-toggle="tooltip"]').tooltip();

function show_preload(){
    $('#model_preload').modal('show');
}
function hide_preload(){
    $('#model_preload').modal('hide');
}

function save_devname(){
    var setting = localStorage.getItem('cicodegen');
    setting = JSON.parse(setting);

    var cicodegen = {
        dev_name :$('#developer_name').val()
    };
    setting = {...setting, ...cicodegen};
    console.log(setting);
    localStorage.setItem('cicodegen', JSON.stringify(setting));

    PNotify.success({
      title: 'Saved',
      text: 'Developer name: '+$('#developer_name').val()
    });

}

function setDBSelect(){
    var setting = localStorage.getItem('cicodegen');
    setting = JSON.parse(setting);

    var cicodegen = {
        db_schema :$('#db_schema').val(),
        db_table :$('#db_table').val()
    };
    setting = {...setting, ...cicodegen};
    console.log(setting);
    localStorage.setItem('cicodegen', JSON.stringify(setting));
}

function getDBSelect(){
    var setting = localStorage.getItem('cicodegen');
    if(setting != null){
        setting = JSON.parse(setting);
        $('#db_schema').val(setting.db_schema).change();
    }
}

function get_devname(){
    var cicodegen = localStorage.getItem('cicodegen');
    if(cicodegen != null){
        var setting = JSON.parse(cicodegen);
        $('#developer_name').val(setting.dev_name);
    }

}
